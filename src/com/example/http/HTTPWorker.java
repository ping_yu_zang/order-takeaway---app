package com.example.http;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Map;

import com.llkj.cm.restfull.exception.RestClientException;
import com.llkj.cm.restfull.network.NetworkConnection;
import com.llkj.cm.restfull.network.NetworkConnection.NetworkConnectionResult;

/**
 * 封装的接口请求类
 * 
 * @author zhang.zk
 * 
 */
public class HTTPWorker {
	/**
	 * 发起网络请求,默认为GET方式
	 * 
	 * @param url
	 *            访问的URL
	 * @param params
	 *            携带参数
	 * @return
	 * @throws IllegalStateException
	 * @throws IOException
	 * @throws URISyntaxException
	 * @throws RestClientException
	 */
	private static String doWork(String url, Map<String, String> params)
			throws IllegalStateException, IOException, URISyntaxException,
			RestClientException {
		return doWork(url, NetworkConnection.METHOD_GET, params);
	}

	/**
	 * 发起网络请求
	 * 
	 * @param url
	 *            访问的URL
	 * @param method
	 *            访问方式
	 * @param params
	 *            携带参数
	 * @return
	 * @throws IllegalStateException
	 * @throws IOException
	 * @throws URISyntaxException
	 * @throws RestClientException
	 */
	private static String doWork(String url, int method,
			Map<String, String> params) throws IllegalStateException,
			IOException, URISyntaxException, RestClientException {
		NetworkConnectionResult wsResult = NetworkConnection
				.retrieveResponseFromService(url, method, params);
//		LogUtil.i(HTTPWorker.class, "params:" + params);
//		LogUtil.i(HTTPWorker.class, "response :" + wsResult.wsResponse);
		return wsResult.wsResponse;
	}

}
