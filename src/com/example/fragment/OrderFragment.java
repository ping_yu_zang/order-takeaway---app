package com.example.fragment;

import java.util.ArrayList;

import com.example.eat.activity.EatOrderView;
import com.example.entity.Order;
import com.example.fun.activity.FunOrderView;
import com.example.seeksubscribe.R;
import com.example.seeksubscribe.Activity.HomeActivity;
import com.example.sing.activity.SingOrderView;
import com.example.utils.ViewPages;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

public class OrderFragment extends BaseFragment{
	private View view;
	private HomeActivity homeActibity;
	private LinearLayout gameLinearLayout;
	private ArrayList<View> viewPagerViews;
	private ViewPages gamePage;
	private ArrayList<Order> eatorderlist;
	private LayoutInflater inflater;
	private View eatorder;
	//唱订单
	//	private View funorder;
	//玩订单
	//	private View singorder;
	private TextView no_pay_order_bt;
	private TextView ok_pay_order_bt;
	private TextView ok_consumption_order_bt;

	public OrderFragment(HomeActivity homeActivity) {
		this.homeActibity=homeActivity;
	}

	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		this.inflater=inflater;
		view=inflater.inflate(R.layout.fragment_eat,null);
		findId();
		initData();
		setListener();
		initPages();
		return view;
	}

	private void findId() {
		gameLinearLayout = (LinearLayout)view.findViewById(R.id.page_linearLayout);
		//未支付订单
		no_pay_order_bt=(TextView)view.findViewById(R.id.no_pay_order_bt);
		//已支付订单
		ok_pay_order_bt=(TextView)view.findViewById(R.id.ok_pay_order_bt);
		//已消费订单
		ok_consumption_order_bt=(TextView)view.findViewById(R.id.ok_consumption_order_bt);
	}
	//先模拟数据
	private void initData() {
		eatorderlist=new ArrayList<Order>();
		for (int i = 0; i < 10; i++) {
			Order eorder = new Order();
			eorder.setOrder_id("1");
			eorder.setOrder_number_tv("88888888");
			eorder.setPat_reality_mone_tv("100");
			eorder.setPay_time("2014-5-12 13:30");
			eorder.setPay_type_tv("付款成功");
			eorder.setPeople_phone_tv("18888888888");
			eorder.setPay_money_tv("88888888");
			eorder.setShop_img("http://192.168.0.5:8080/abc.jpg");
			eorder.setShop_name_tv("咖喱大叔");
			eorder.setAddress_receiving("陕西省西安市高新区科技二路佳贝大厦");
			eatorderlist.add(eorder);
		}
	}

	private void setListener() {
		no_pay_order_bt.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				ok_pay_order_bt.setTextColor(Color.parseColor("#666666"));
				ok_pay_order_bt.setBackgroundColor(0);
				ok_consumption_order_bt.setTextColor(Color.parseColor("#666666"));
				ok_consumption_order_bt.setBackgroundColor(0);
				no_pay_order_bt.setTextColor(Color.parseColor("#ffffff"));
				no_pay_order_bt.setBackgroundResource(R.drawable.yuanjiao_huang_biankuang_huangzuobianyuan_fill);
			}
		});
		ok_pay_order_bt.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				ok_pay_order_bt.setTextColor(Color.parseColor("#ffffff"));
				ok_pay_order_bt.setBackgroundResource(R.drawable.yuanjiao_huang_biankuang_huangwubianjiao_fill);
				ok_consumption_order_bt.setTextColor(Color.parseColor("#666666"));
				ok_consumption_order_bt.setBackgroundColor(0);
				no_pay_order_bt.setTextColor(Color.parseColor("#666666"));
				no_pay_order_bt.setBackgroundColor(0);
			}
		});
		ok_consumption_order_bt.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				ok_pay_order_bt.setTextColor(Color.parseColor("#666666"));
				ok_pay_order_bt.setBackgroundColor(0);
				ok_consumption_order_bt.setTextColor(Color.parseColor("#ffffff"));
				ok_consumption_order_bt.setBackgroundResource(R.drawable.yuanjiao_huang_biankuang_huangyoubianyuan_fill);
				no_pay_order_bt.setTextColor(Color.parseColor("#666666"));
				no_pay_order_bt.setBackgroundColor(0);
			}
		});

	}

	private void initPages() {
		//将来是要换数据的
		viewPagerViews = new ArrayList<View>();
		eatorder=new EatOrderView(homeActibity, inflater.inflate(R.layout.layout_eat_list_order, null), eatorderlist).view;
		//唱订单		funorder=new FunOrderView(homeActibity, inflater.inflate(R.layout.layout_fun_list_order, null), eatorderlist).view;
		//玩订单	    singorder=new SingOrderView(homeActibity, inflater.inflate(R.layout.layout_sing_list_order, null), eatorderlist).view;
		viewPagerViews.add(eatorder);
		//		viewPagerViews.add(funorder);
		//		viewPagerViews.add(singorder);
		// viewPagerViews.add(new BooksMsgNoticeView(BooksMessageActivity.this,
		// inflater.inflate(R.layout.books_notice, null)).view);
		gamePage = new ViewPages(homeActibity, gameLinearLayout, viewPagerViews, 0);
	}
}
