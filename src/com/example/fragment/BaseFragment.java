package com.example.fragment;


import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import com.example.http.NetworkErrorLog;
import com.example.http.PoCRequestManager;
import com.example.http.PoCRequestManager.OnRequestFinishedListener;
import com.example.http.PoCService;
import com.example.seeksubscribe.R;
import com.example.utils.LogUtil;
import com.example.utils.ToastUtil;

public class BaseFragment extends Fragment {
	protected View rootView;
	protected PoCRequestManager mRequestManager;
	protected ProgressDialog waitDialog;
	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		return rootView;
	}
	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		mRequestManager = PoCRequestManager.from(activity);
	}
	@Override
	public void onDestroyView() {
		super.onDestroyView();
		// mRequestManager.removeOnRequestFinishedListener(this);
		dismissDialog();
	}

	@Override
	public void onResume() {
		super.onResume();
	}

	@Override
	public void onStop() {
		super.onStop();

	}

	/**
	 * 全局等待对话框
	 */
	public void showWaitDialog() {
		try {
			if (waitDialog == null) {
				waitDialog = new ProgressDialog(getActivity());
				waitDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
				waitDialog.setCanceledOnTouchOutside(false);
			}
			ImageView view = new ImageView(getActivity());
			view.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT,
					LayoutParams.WRAP_CONTENT));
			Animation loadAnimation = AnimationUtils.loadAnimation(
					getActivity(), R.anim.rotate);
			view.startAnimation(loadAnimation);
			loadAnimation.start();
			view.setImageResource(R.drawable.loading);
			// waitDialog.setCancelable(false);
			if (!waitDialog.isShowing()) {
				waitDialog.show();
				waitDialog.setContentView(view);
			}
			LogUtil.i("waitDialong.......");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	/**
	 * 关闭全局等待对话框,请稍后
	 */
	public void dismissDialog() {
		if (waitDialog != null && waitDialog.isShowing()) {
			waitDialog.dismiss();
			waitDialog = null;
		}
	}
}
