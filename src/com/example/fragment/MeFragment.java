package com.example.fragment;

import org.apache.http.util.LangUtils;

import com.example.seeksubscribe.R;
import com.example.seeksubscribe.Activity.ActivityServerCenter;
import com.example.seeksubscribe.Activity.AddSiteActivity;
import com.example.seeksubscribe.Activity.AdviceActivity;
import com.example.seeksubscribe.Activity.HomeActivity;
import com.example.seeksubscribe.Activity.LonginActivity;
import com.example.seeksubscribe.Activity.MessageActivity;
import com.example.seeksubscribe.Activity.MycolletvActivity;
import com.example.seeksubscribe.Activity.RegisterActivity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;

public class MeFragment extends BaseFragment{
	private View view;
	private HomeActivity homeActibity;
	private LinearLayout next_ll;
	private LinearLayout opinion_ll;
	private LinearLayout server_center_ll;
	private LinearLayout distribution_ll;
	private LinearLayout news_ll;
	private LinearLayout set_image_ll;
	private LinearLayout me_collect_ll;
	private Button longin_bt;
	private Button register_bt;

	public MeFragment(HomeActivity homeActivity) {
		this.homeActibity=homeActivity;
	}

	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		view=inflater.inflate(R.layout.fragment_me, null);
		findId();
		initData();
		setListener();
		initPages();
		return view;
	}

	private void findId() {
		//注册
		register_bt=(Button)view.findViewById(R.id.register_bt);
		//登录
		longin_bt=(Button)view.findViewById(R.id.longin_bt);
		//收藏
		me_collect_ll=(LinearLayout)view.findViewById(R.id.me_collect_ll);
		//消息通知
		news_ll=(LinearLayout)view.findViewById(R.id.news_ll);
		//送餐地址
		distribution_ll=(LinearLayout)view.findViewById(R.id.distribution_ll);
		//服务中心
		server_center_ll=(LinearLayout)view.findViewById(R.id.server_center_ll);
		//投诉建议
		opinion_ll=(LinearLayout)view.findViewById(R.id.opinion_ll);
		//退出登录
		next_ll=(LinearLayout)view.findViewById(R.id.next_ll);
		
	}

	private void initData() {
		
		
	}

	private void setListener() {
		//注册
		register_bt.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent i = new Intent(homeActibity, RegisterActivity.class);
				startActivity(i);
			}
		});
		//服务中心
		server_center_ll.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent i = new Intent(homeActibity, ActivityServerCenter.class);
				startActivity(i);
				
			}
		});
		//登陆
		longin_bt.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent i = new Intent(homeActibity, LonginActivity.class);
				startActivity(i);
			}
		});
		//收藏
		me_collect_ll.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent i = new Intent(homeActibity, MycolletvActivity.class);
				startActivity(i);
			}
		});
		//送餐地址
		distribution_ll.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent i = new Intent(homeActibity, AddSiteActivity.class);
				i.putExtra("mark", 0);
				startActivity(i);
			}
		});
		//投诉建议
		opinion_ll.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent i = new Intent(homeActibity, AdviceActivity.class);
				startActivity(i);
			}
		});
		//退出登录
		next_ll.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent i = new Intent(homeActibity, LonginActivity.class);
				startActivity(i);
				homeActibity.finish();
			}
		});
		//消息通知
		news_ll.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent i=new Intent(homeActibity, MessageActivity.class);
				startActivity(i);
			}
		});
		
	}

	private void initPages() {
		// TODO Auto-generated method stub
		
	}
}
