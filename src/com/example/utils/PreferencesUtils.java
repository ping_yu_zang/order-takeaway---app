package com.example.utils;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * 偏好设置�? 获取和存储应用的偏好设置
 * 
 * @author csl
 * 
 */
public class PreferencesUtils {
	private String TAG = "Preferences";
	private static PreferencesUtils mPreferences;
	public SharedPreferences mSettings;
	private SharedPreferences.Editor mEditor;
	public static final String STORE_NAME = "Settings";
	public Context context;
	private PreferencesUtils(Context context) {
		this.context=context;
		mSettings = context.getSharedPreferences(STORE_NAME,
				Context.MODE_WORLD_WRITEABLE);
		mEditor = mSettings.edit();
	}
	

	/**
	 * 获取该类实例对象
	 * 
	 * @return
	 */
	public static PreferencesUtils getInstance(Context context) {
		if (mPreferences == null) {
			mPreferences = new PreferencesUtils(context);
		}
		return mPreferences;
	}

	
	/**
	 * set preference  String
	 */
	public static void setPrefString(Context context,String key,String value){
		
		SharedPreferences mSettings;
		mSettings = context.getSharedPreferences(STORE_NAME,
				Context.MODE_WORLD_WRITEABLE);
		mSettings.edit().putString(key, value).commit();
	}
	
	
	/**
	 * get preference  String
	 */
	public static String getPrefString(Context context,String key,String defaultValue){
		SharedPreferences mSettings;
		mSettings = context.getSharedPreferences(STORE_NAME,
				Context.MODE_WORLD_WRITEABLE);
		return mSettings.getString(key, defaultValue);
	}
	
	/**
	 * set preference  int
	 */
	public static void setPrefInt(Context context,String key,int value){
		
		SharedPreferences mSettings;
		mSettings = context.getSharedPreferences(STORE_NAME,
				Context.MODE_WORLD_WRITEABLE);
		mSettings.edit().putInt(key, value).commit();
	}
	
	
	/**
	 * get preference  int
	 */
	public static int getPrefInt(Context context,String key,int defaultValue){
		SharedPreferences mSettings;
		mSettings = context.getSharedPreferences(STORE_NAME,
				Context.MODE_WORLD_WRITEABLE);
		return mSettings.getInt(key, defaultValue);
	}
	
	/**
	 * set preference  bool
	 */
	public static void setPrefBool(Context context,String key,boolean value){
		
		SharedPreferences mSettings;
		mSettings = context.getSharedPreferences(STORE_NAME,
				Context.MODE_WORLD_WRITEABLE);
		mSettings.edit().putBoolean(key, value).commit();
	}
	
	
	/**
	 * get preference  bool
	 */
	public static boolean getPrefBool(Context context,String key,boolean defaultValue){
		SharedPreferences mSettings;
		mSettings = context.getSharedPreferences(STORE_NAME,
				Context.MODE_WORLD_WRITEABLE);
		return mSettings.getBoolean(key, defaultValue);
	}
	
	
	/**
	 * set sessionId
	 */
	public void setSessionId(String sessionId)
	{
		mEditor.putString(ConstantsUtil.SESSION_ID, sessionId);
		mEditor.commit();
	}
	/**
	 * getsessionId
	 */
	public String getSessionId()
	{
		if ( mSettings.getString(ConstantsUtil.SESSION_ID, "")==null) {
			return "";
		}
		return mSettings.getString(ConstantsUtil.SESSION_ID, "");
	}
	
}
