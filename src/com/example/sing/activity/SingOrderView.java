package com.example.sing.activity;

import java.io.Serializable;
import java.util.ArrayList;

import com.example.eat.activity.ActivityEatOrderOK;
import com.example.eat.adapter.EatOrderAdapter;
import com.example.entity.Order;
import com.example.seeksubscribe.R;
import com.example.seeksubscribe.Activity.HomeActivity;

import android.content.Intent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.AdapterView.OnItemClickListener;


public class SingOrderView {
	private HomeActivity homeActivity;
	public View view;
	private ArrayList<Order> singorder;
	private ListView order_list;

	public SingOrderView(HomeActivity homeActivity, View view, ArrayList<Order> order){
		this.homeActivity=homeActivity;
		this.view=view;
		this.singorder=order;
		findview();
		initdata();
		init();
		listener();
	}
	private void findview() {
		order_list=(ListView)view.findViewById(R.id.order_list);
	}

	private void initdata() {
		
	}

	private void init() {
		EatOrderAdapter eatatapter = new EatOrderAdapter(homeActivity, singorder);
		order_list.setAdapter(eatatapter);
	}

	private void listener() {
		order_list.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				Order singOrder = singorder.get(position);
				Intent i = new Intent(homeActivity, ActivitySingOrderOK.class);
				i.putExtra("singOrder", (Serializable)singOrder);
				homeActivity.startActivity(i);
			}
		});
		
	}
}
