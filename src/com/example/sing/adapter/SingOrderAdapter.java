package com.example.sing.adapter;

import java.util.ArrayList;

import com.example.entity.Order;
import com.example.seeksubscribe.R;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class SingOrderAdapter extends BaseAdapter{

	private Context context;
	private LayoutInflater inflater;
	private ArrayList<Order> list;

	public SingOrderAdapter(Context context,ArrayList<Order> list){
		this.context=context;
		this.list=list;
		inflater=LayoutInflater.from(context);
		
	}
	@Override
	public int getCount() {
		return list.size();
	}

	@Override
	public Object getItem(int position) {
		return list.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		handerView hanger=null;
		if(convertView==null){
			convertView=inflater.inflate(R.layout.item_sing_list_order, null);
			hanger=new handerView();
			hanger.shop_name_tv=(TextView) convertView.findViewById(R.id.shop_name_tv);
			hanger.pay_type_tv=(TextView) convertView.findViewById(R.id.pay_type_tv);
			hanger.pay_time=(TextView) convertView.findViewById(R.id.pay_time);
			hanger.pay_money_tv=(TextView) convertView.findViewById(R.id.pay_money_tv);
			hanger.shop_img=(ImageView) convertView.findViewById(R.id.shop_img);
			convertView.setTag(hanger);
		}else {
			hanger=(handerView) convertView.getTag();
		}
		Order order = list.get(position);
		hanger.shop_name_tv.setText(order.getShop_name_tv());
		hanger.pay_type_tv.setText(order.getPay_type_tv());
		hanger.pay_time.setText(order.getPay_time());
		hanger.pay_money_tv.setText(order.getPay_money_tv());
		Bitmap bitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.restaurant_gali2);
		hanger.shop_img.setImageBitmap(bitmap);
		
		return convertView;
	}
	class handerView{
		TextView shop_name_tv;
		TextView pay_type_tv;
		TextView pay_time;
		TextView pay_money_tv;
		ImageView shop_img;
	}
}
