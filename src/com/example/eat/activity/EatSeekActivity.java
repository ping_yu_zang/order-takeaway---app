package com.example.eat.activity;

import java.util.ArrayList;

import com.example.dao.MyApplication;
import com.example.eat.adapter.EatSeekShopAdapter;
import com.example.eat.adapter.EatSortTypeAdapter;
import com.example.eat.adapter.EatSortTypeToAdapter;
import com.example.entity.ShopLv;
import com.example.seeksubscribe.R;
import com.example.seeksubscribe.Activity.BaseActivity;
import com.example.seeksubscribe.Activity.CaptureActivity;
import com.example.utils.ToastUtil;

import android.content.Intent;
import android.database.DataSetObserver;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class EatSeekActivity extends BaseActivity implements OnClickListener{
	//回退按钮
	private RelativeLayout title_return_rl;
	//回退旁边文本显示
	private TextView title_location_tv;
	//二维码
	private RelativeLayout title_toreight_rl;
	//查询
	private RelativeLayout title_reight_rl;
	//全部分类
	private LinearLayout eat_all_classify_ll;
	//排序分类
	private LinearLayout eat_sort_classify_ll;
	//收起箭头
	private ImageView eat_all_classify_iv;
	private ImageView eat_sort_classify_iv;
	//各类餐厅的listView
	private ListView eat_seek_lv;
	private ShopLv shopLv;
	private ArrayList<ShopLv> ShoplvArraylist;
	private PopupWindow pw;
	private EatSortTypeToAdapter eatSortTypeToAdapter;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		requestWindowFeature(1);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_eat_seek);
		findviewByid();
		initDate();
		init();
		initListener();
	}


	private void initDate() {
		//正式写 通过网络获取
		shopLv=new ShopLv();
		ShoplvArraylist=new ArrayList<ShopLv>();
		for(int i=0;i<10;i++){
			shopLv.setShopId("108");
			shopLv.setShopMoney("45");
			shopLv.setShopName("咖喱大叔");
			shopLv.setShopType("越南菜");
			shopLv.setShopRange("10km");
			ShoplvArraylist.add(shopLv);
		}
	}
	private void init() {
		EatSeekShopAdapter eatShopAdapter = new EatSeekShopAdapter(ShoplvArraylist, this);
		eat_seek_lv.setAdapter(eatShopAdapter);
	}

	private void initListener() {
		title_return_rl.setOnClickListener(this);
		title_toreight_rl.setOnClickListener(this);
		title_reight_rl.setOnClickListener(this);
		eat_all_classify_ll.setOnClickListener(this);
		eat_sort_classify_ll.setOnClickListener(this);
		eat_seek_lv.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View view, int position,
					long arg3) {
				MyApplication ma = MyApplication.getInstance();
				//重新选择商店清下总价和菜品集合
				ma.sun_money="0";
				//每次点击初始化
				ma.disherList.clear();
				//标记重新获取数据
				ma.mark=2;
				String shopId= ShoplvArraylist.get(position).getShopId();
				Intent i=new Intent(EatSeekActivity.this, EatSeekShopActivity.class);
				Bundle shopbundle = new Bundle();
				shopbundle.putInt("shopld",Integer.parseInt(shopId));
				i.putExtra("shopbundle", shopbundle);
				startActivityForResult(i, 100);
			}
		});
		}

	private void findviewByid() {
		title_return_rl=(RelativeLayout)findViewById(R.id.title_return_rl);
		title_reight_rl=(RelativeLayout)findViewById(R.id.title_reight_rl);
		title_toreight_rl=(RelativeLayout)findViewById(R.id.title_toreight_rl);
		title_location_tv=(TextView)findViewById(R.id.title_location_tv);


		eat_all_classify_ll=(LinearLayout)findViewById(R.id.eat_all_classify_ll);
		eat_all_classify_iv=(ImageView)findViewById(R.id.eat_all_classify_iv);
		eat_sort_classify_ll=(LinearLayout)findViewById(R.id.eat_sort_classify_ll);
		eat_sort_classify_iv=(ImageView)findViewById(R.id.eat_sort_classify_iv);
		eat_seek_lv=(ListView)findViewById(R.id.eat_seek_lv);
	}
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		switch (requestCode) {
		case 100:
			System.out.println("进入餐厅回退监听,resultCode,101="
		+resultCode);
			break;
		}
	}
	@Override
	public void onClick(View arg0) {
		switch (arg0.getId()) {
		case R.id.title_return_rl:
			finish();
			break;
		case R.id.title_toreight_rl:
			//跳二维码
			Intent erweimain = new Intent(EatSeekActivity.this, CaptureActivity.class);
			startActivity(erweimain);
			break;
		case R.id.title_reight_rl:
			//跳搜索饭店
			Intent i = new Intent(EatSeekActivity.this, EatEaterySeekActivity.class);
			startActivity(i);
			break;
		case R.id.eat_all_classify_ll:
			showChoiseDialog(1);
			break;
		case R.id.eat_sort_classify_ll:
			showChoiseDialog(2);
			break;
		}
	}
	/**
	 * 选择发布的类型,mark标记1是选类型，2是分类
	 */
	private void showChoiseDialog(int mark) {
		if (pw != null && pw.isShowing()) {
			pw.dismiss();
		} else {
			final View view = LayoutInflater.from(EatSeekActivity.this).inflate(
					R.layout.dialog_eat_sort, null);
			pw = new PopupWindow(view, ViewGroup.LayoutParams.MATCH_PARENT,
					ViewGroup.LayoutParams.MATCH_PARENT);
			ListView eat_sort_type_lv = (ListView)view.findViewById(R.id.eat_sort_type_lv);
			final ListView eat_sort_type_to_lv = (ListView)view.findViewById(R.id.eat_sort_type_to_lv);
			final ArrayList<String> typeAll = new ArrayList<String>();
			if (mark==1) {
				eat_sort_type_to_lv.setVisibility(View.VISIBLE);
				//虚拟数据，以后联网获取
				typeAll.add("中餐");
				typeAll.add("西餐");
				typeAll.add("意大利餐");
				typeAll.add("日本料理");
				typeAll.add("火锅");
				typeAll.add("自助餐");
				typeAll.add("烧烤");
				eat_sort_type_lv.setAdapter(new EatSortTypeAdapter(typeAll, EatSeekActivity.this));
				eatSortTypeToAdapter = new EatSortTypeToAdapter(EatSeekActivity.this);
				eat_sort_type_lv.setOnItemClickListener(new OnItemClickListener() {

					@Override
					public void onItemClick(AdapterView<?> arg0, View arg1,
							int arg2, long arg3) {
						switch (arg2) {
						case 0:
							ToastUtil.makeText(EatSeekActivity.this, "中餐", 0);
							ArrayList<String> EatChina = new ArrayList<String>();
							//虚拟数据，以后联网获取
							EatChina.add("粤菜");
							EatChina.add("湖南菜");
							EatChina.add("东北菜");
							EatChina.add("川菜");
							 eatSortTypeToAdapter.setData(EatChina);
								//更新adapter
//							eatSortTypeToAdapter.notifyDataSetInvalidated();
							 eat_sort_type_to_lv.setOnItemClickListener(new OnItemClickListener() {

								@Override
								public void onItemClick(AdapterView<?> parent,
										View view, int position, long id) {
									switch (position) {
									case 0:
										toast("粤菜", EatSeekActivity.this);
										pw.dismiss();
										break;
									}
									
								}
							});
							break;
						case 1:
							ToastUtil.makeText(EatSeekActivity.this, "西餐", 0);
							ArrayList<String> EatUSA = new ArrayList<String>();
							//虚拟数据，以后联网获取
							EatUSA.add("汉堡包");
							EatUSA.add("肯德基");
							EatUSA.add("麦当劳");
							EatUSA.add("牛排");
							eatSortTypeToAdapter.setData(EatUSA);
								//更新adapter
//							 eatSortTypeToAdapter.notifyDataSetInvalidated();
							 eat_sort_type_to_lv.setOnItemClickListener(new OnItemClickListener() {

									@Override
									public void onItemClick(AdapterView<?> parent,
											View view, int position, long id) {
										switch (position) {
										case 0:
											toast("汉堡包", EatSeekActivity.this);
											pw.dismiss();
											break;
										}
										
									}
								});
							break;
						case 2:
							ToastUtil.makeText(EatSeekActivity.this, "意大利餐", 0);
							break;
						case 3:
							ToastUtil.makeText(EatSeekActivity.this, "日本料理", 0);
							break;
						case 4:
							ToastUtil.makeText(EatSeekActivity.this, "火锅", 0);
							break;
						case 5:
							ToastUtil.makeText(EatSeekActivity.this, "自助餐", 0);
							break;
						case 6:
							ToastUtil.makeText(EatSeekActivity.this, "烧烤", 0);
							break;
						}
						eat_sort_type_to_lv.setAdapter(eatSortTypeToAdapter);
					}
				});
			}else {
				eat_sort_type_to_lv.setVisibility(View.GONE);
				//虚拟数据，以后联网获取
				typeAll.add("离我最近");
				typeAll.add("评价最高");
				typeAll.add("销量最高");
				typeAll.add("新品上架");
				eat_sort_type_lv.setAdapter(new EatSortTypeAdapter(typeAll, EatSeekActivity.this));
				eat_sort_type_lv.setOnItemClickListener(new OnItemClickListener() {

					@Override
					public void onItemClick(AdapterView<?> parent, View view,
							int position, long id) {
						switch (position) {
						case 0:
							toast("位置排序", EatSeekActivity.this);
							pw.dismiss();
							break;

						default:
							break;
						}
						
					}
				});
			}
				pw.setOutsideTouchable(true);
				pw.setFocusable(true);
				view.setOnTouchListener(new OnTouchListener() {
					@Override
					public boolean onTouch(View v, MotionEvent event) {
						int height = view.findViewById(R.id.eat_all_sort_ll).getTop();
						int y = (int) event.getY();
						if (y < height) {
							pw.dismiss();
						}
						return false;
					}
				});
				// 此行代码可以在返回键按下的时候,使pw消失.
				pw.setBackgroundDrawable(new BitmapDrawable());
				pw.setAnimationStyle(R.style.AnimBottom);
				pw.showAtLocation(EatSeekActivity.this.getWindow().getDecorView(),
						Gravity.BOTTOM, 0, 0);
		}
	}
}
