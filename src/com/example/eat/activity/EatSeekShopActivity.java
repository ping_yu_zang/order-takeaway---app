package com.example.eat.activity;

import java.util.ArrayList;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.test.TouchUtils;
import android.text.method.Touch;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.entity.ShopLv;
import com.example.seeksubscribe.R;
import com.example.seeksubscribe.Activity.BaseActivity;
import com.example.utils.ToastUtil;

//饭店
public class EatSeekShopActivity extends BaseActivity implements OnClickListener{
	private TextView title_location_tv;
	private ImageView title_reight_iv;
	private ImageView title_toreight_iv;
	private RelativeLayout title_return_rl;
	private ImageView eat_shap_picture_iv;
	private LinearLayout eat_subscribe_ll;
	private LinearLayout eat_order_ll;
	private LinearLayout shop_activity_ll;
	private TextView shop_activity_explain_tv;
	private ImageView shop_activity_image_iv;
	private TextView shop_cost_price_tv;
	private TextView shop_activity_price_tv;
	private LinearLayout shao_location_ll;
	private LinearLayout shao_number_ll;
	private TextView shao_location_tv;
	private TextView shao_number_tv;
	private TextView shao_explain_tv;
	private ImageView evaluate_man_head_iv;
	private ImageView evaluate_man_head_iv2;
	private TextView evaluate_man_name_tv;
	private TextView evaluate_man_name_tv2;
	private TextView evaluate_man_content_tv;
	private TextView evaluate_man_content_tv2;
	private TextView evaluate_man_time_tv;
	private TextView evaluate_man_time_tv2;
	private TextView shop_evaluate_more_tv;
	private TextView like_bt;
	//标记 1喜欢店铺，2不喜欢店铺
			int mark = 1;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		requestWindowFeature(1);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_eat_seekshop);
		findviewByid();
		intitData();
		init();
		initlistenter();
	}


	private void findviewByid() {
		//标题文字
		title_location_tv=(TextView)findViewById(R.id.title_location_tv);
		//从右往左第1个图标（收藏）
		title_reight_iv=(ImageView)findViewById(R.id.title_reight_iv);
		//从右往左第2个图标（分享）
		title_toreight_iv=(ImageView)findViewById(R.id.title_toreight_iv);
		//返回
		title_return_rl=(RelativeLayout)findViewById(R.id.title_return_rl);
		//店铺头图
		eat_shap_picture_iv=(ImageView)findViewById(R.id.eat_shap_picture_iv);
		//预订桌位
		eat_subscribe_ll=(LinearLayout)findViewById(R.id.eat_subscribe_ll);
		//点菜
		eat_order_ll=(LinearLayout)findViewById(R.id.eat_order_ll);
		//商家活动布局（以后得判断店家做不做优惠，不做的话隐藏此布局）
		shop_activity_ll=(LinearLayout)findViewById(R.id.shop_activity_ll);
		//活动说明
		shop_activity_explain_tv=(TextView)findViewById(R.id.shop_activity_explain_tv);
		//活动图片
		shop_activity_image_iv=(ImageView)findViewById(R.id.shop_activity_image_iv);
		//原价（到时候看是不是做价格活动）
		shop_cost_price_tv=(TextView)findViewById(R.id.shop_cost_price_tv);
		//原价（到时候看是不是做价格活动）
		shop_activity_price_tv=(TextView)findViewById(R.id.shop_activity_price_tv);
		//店铺位置布局
		shao_location_ll=(LinearLayout)findViewById(R.id.shao_location_ll);
		//店铺电话布局
		shao_number_ll=(LinearLayout)findViewById(R.id.shao_number_ll);
		//店铺位置
		shao_location_tv=(TextView)findViewById(R.id.shao_location_tv);
		//店铺电话
		shao_number_tv=(TextView)findViewById(R.id.shao_number_tv);
		//店铺说明
		shao_explain_tv=(TextView)findViewById(R.id.shao_explain_tv);
		//评价者头像
		evaluate_man_head_iv=(ImageView)findViewById(R.id.evaluate_man_head_iv);
		evaluate_man_head_iv2=(ImageView)findViewById(R.id.evaluate_man_head_iv2);
		//评价者名字
		evaluate_man_name_tv=(TextView)findViewById(R.id.evaluate_man_name_tv);
		evaluate_man_name_tv2=(TextView)findViewById(R.id.evaluate_man_name_tv2);
		//评论内容
		evaluate_man_content_tv=(TextView)findViewById(R.id.evaluate_man_content_tv);
		evaluate_man_content_tv2=(TextView)findViewById(R.id.evaluate_man_content_tv2);
		//评论时间
		evaluate_man_time_tv=(TextView)findViewById(R.id.evaluate_man_time_tv);
		evaluate_man_time_tv2=(TextView)findViewById(R.id.evaluate_man_time_tv2);
		//点击查看更多评论
		shop_evaluate_more_tv=(TextView)findViewById(R.id.shop_evaluate_more_tv);
		//我喜欢
		like_bt=(TextView)findViewById(R.id.like_bt);
	}
	private void init() {
		title_location_tv.setText("店铺详情");
		//收藏
		title_reight_iv.setBackgroundDrawable(getResources().getDrawable(R.drawable.collect));
		//分享
		title_toreight_iv.setBackgroundDrawable(getResources().getDrawable(R.drawable.share));
	}

	private void intitData() {
		Bundle shopbun = getIntent().getBundleExtra("shopbundle");
		System.out.println("获取饭店ID="+shopbun.getInt("shopld"));
	}

	private void initlistenter() {
		title_return_rl.setOnClickListener(this);
		eat_subscribe_ll.setOnClickListener(this);
		eat_order_ll.setOnClickListener(this);
		shop_activity_ll.setOnClickListener(this);
		title_toreight_iv.setOnClickListener(this);
		title_reight_iv.setOnClickListener(this);
		shao_number_ll.setOnClickListener(this);
		like_bt.setOnClickListener(this);
	}


	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.title_return_rl:
			Intent intent = new Intent();
			intent.putExtra("mark111", "mark111");
			setResult(101);
			finish();
			break;
		case R.id.shao_number_ll:
			String number=shao_number_tv.getText().toString();
			if (null!=number&&number.length()>0) {
				//用intent启动拨打电话  
				Intent inten = new Intent(Intent.ACTION_CALL,Uri.parse("tel:"+number));  
				startActivity(inten);  
			}else {
				toast("此店铺为上传电话", EatSeekShopActivity.this);
			}
			break;
		case R.id.eat_subscribe_ll:
			Intent i=new Intent(this, EatShopCustomTableActivity.class);
			startActivity(i);
			ToastUtil.makeText(this, "预订桌位",0);
			break;
		case R.id.eat_order_ll:
			Intent in=new Intent(this, EatChooseFoodActivity.class);
			startActivity(in);
			break;
		case R.id.shop_activity_ll:
//			ToastUtil.makeText(this, "更多优惠",0);
			Intent inte=new Intent(this, EatShopEventActivity.class);
			startActivity(inte);
			break;
		case R.id.title_toreight_iv:
			//分享   判断获取数据成功后分享
			if (true) {
				Intent iFX=new Intent(Intent.ACTION_SEND);    
				iFX.setType("image/*");    
				iFX.putExtra(Intent.EXTRA_SUBJECT, "Share");    
				iFX.putExtra(Intent.EXTRA_TEXT, "找外卖：那个那个店，推荐");    
				iFX.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);    
				startActivity(Intent.createChooser(iFX, getTitle()));    
			}  
			break;
		case R.id.title_reight_iv:
			//发送服务器用户收藏
			toast("收藏", this);
			break;
		case R.id.like_bt:
			switch (mark) {
			case 1:
				//发送喜欢店铺，修改标记
				like_bt.setBackground(getResources().getDrawable(R.drawable.collcetion_ok));
				mark=2;
				break;
			case 2:
				like_bt.setBackground(getResources().getDrawable(R.drawable.collection_no));
				//不喜欢发送取消点赞标记
				mark=1;
				break;
			}
			break;
		}
	}
}
