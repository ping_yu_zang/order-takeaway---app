package com.example.eat.activity;

import java.util.ArrayList;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.dao.MyApplication;
import com.example.eat.adapter.EatSeekShopAdapter;
import com.example.entity.ShopLv;
import com.example.seeksubscribe.R;
import com.example.seeksubscribe.Activity.BaseActivity;

public class EatEaterySeekActivity extends BaseActivity {

	private RelativeLayout title_return_rl;
	private EditText find_eatery_et;
	private TextView title_reight_tv;
	private ListView eatery_lv;
	private ArrayList<ShopLv> ShoplvArraylist;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		requestWindowFeature(1);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_seek_eatery);
		findView();
		Listener();
	}

	private void findView() {
		//回退布局
		title_return_rl=(RelativeLayout)findViewById(R.id.title_return_rl);
		//餐馆输入
		find_eatery_et=(EditText)findViewById(R.id.find_eatery_et);
		//查询按钮
		title_reight_tv=(TextView)findViewById(R.id.title_reight_tv);
		//查询出来的饭店集合
		eatery_lv=(ListView)findViewById(R.id.eatery_lv);

	}
	private void Listener() {
		title_return_rl.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				finish();
			}
		});
		title_reight_tv.setOnClickListener(new OnClickListener() {
			

			@Override
			public void onClick(View v) {
				String eateryNameString=find_eatery_et.getText().toString();
				if (null!=eateryNameString&&eateryNameString.length()>0) {
					showWaitDialog();
					//正式写 通过网络获取数据
					ShopLv shopLv = new ShopLv();
					 ShoplvArraylist = new ArrayList<ShopLv>();
					for(int i=0;i<10;i++){
						shopLv.setShopId("108");
						shopLv.setShopMoney("45");
						shopLv.setShopName("咖喱大叔");
						shopLv.setShopType("越南菜");
						shopLv.setShopRange("10km");
						ShoplvArraylist.add(shopLv);
					}
					EatSeekShopAdapter adapter = new EatSeekShopAdapter(ShoplvArraylist, EatEaterySeekActivity.this);
					eatery_lv.setAdapter(adapter);
					dismissDialog();
				}else {
					toast("请输入您要查找的饭店名字",EatEaterySeekActivity.this);
				}
				
			}
		});
		eatery_lv.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				MyApplication ma = MyApplication.getInstance();
				//重新选择商店清下总价和菜品集合
				ma.sun_money="0";
				//每次点击初始化
				ma.disherList.clear();
				//标记重新获取数据
				ma.mark=2;
				String shopId= ShoplvArraylist.get(position).getShopId();
				Intent i=new Intent(EatEaterySeekActivity.this, EatSeekShopActivity.class);
				Bundle shopbundle = new Bundle();
				shopbundle.putInt("shopld",Integer.parseInt(shopId));
				i.putExtra("shopbundle", shopbundle);
				startActivity(i);
			}
		});
	}


}
