package com.example.eat.activity;

import java.util.ArrayList;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;

import com.example.dao.MyApplication;
import com.example.eat.adapter.EatChooseFoodAdapter;
import com.example.eat.adapter.TimeChooseAdapter;
import com.example.entity.Dishes;
import com.example.seeksubscribe.R;
import com.example.seeksubscribe.Activity.BaseActivity;
import com.example.utils.ToastUtil;
/**
 * 食物选择
 * @author User
 *
 */
public class EatChooseFoodActivity extends BaseActivity implements OnClickListener{


	private TextView title_location_tv;
	private ImageView title_reight_iv;
	private ImageView title_toreight_iv;
	private RelativeLayout title_return_rl;
	private LinearLayout eat_all_classify_ll;
	private TextView eat_food_classify_tv;
	private ListView eat_seek_lv;
	private TextView total_money;
	private Button order_disher_bt;
	private PopupWindow pw;
	private EatChooseFoodAdapter ECFadapter;
	private MyApplication myapp;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		requestWindowFeature(1);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_eat_choose_food);
		myapp=MyApplication.getInstance();
		findviewByid();
		intitData();
		init();
		initlistenter();
	}
	public void test(int position,int i){
		if(!total_money.getText().toString().equals("")){
			int money=Integer.parseInt(total_money.getText().toString());
			switch (i) {
			case 1:
				total_money.setText(money+position+"");
				break;
			case 2:
				if(money-position>=0){
					total_money.setText(money-position+"");
				}else {
					total_money.setText("0");
				}
				break;
			}
		}else {
			total_money.setText("0");
		}
	}
	private void findviewByid() {
		//标题文字
		title_location_tv=(TextView)findViewById(R.id.title_location_tv);
		//从右往左第1个图标（收藏）
		title_reight_iv=(ImageView)findViewById(R.id.title_reight_iv);
		//从右往左第2个图标（分享）
		title_toreight_iv=(ImageView)findViewById(R.id.title_toreight_iv);
		//返回
		title_return_rl=(RelativeLayout)findViewById(R.id.title_return_rl);
		//分类
		eat_all_classify_ll=(LinearLayout)findViewById(R.id.eat_all_classify_ll);
		//分类显示
		eat_food_classify_tv=(TextView)findViewById(R.id.eat_food_classify_tv);
		//共计多少钱
		total_money=(TextView)findViewById(R.id.total_money);
		//分类listView
		eat_seek_lv=(ListView)findViewById(R.id.eat_seek_lv);
		//选好了
		order_disher_bt=(Button)findViewById(R.id.order_disher_bt);
	}

	private void intitData() {
		//如果不是1是重新点的商品，要重新获取数据。如果是1是修改菜品，不需要重新获取数据
		if(myapp.mark!=1){
			for(int i=0;i<10;i++){
				Dishes disher = new Dishes();
				disher.setId("0");
				disher.setName("萝卜青菜");
				disher.setOriginalCost("5");
				disher.setCurrentCost("3");
				disher.setType("热菜");
				disher.setNumber(0);
				myapp.disherList.add(disher);
			}
		}
	}

	private void init() {
		total_money.setText(myapp.sun_money);
		title_reight_iv.setVisibility(View.GONE);
		title_toreight_iv.setVisibility(View.GONE);
		ECFadapter = new EatChooseFoodAdapter(this);
		eat_seek_lv.setAdapter(ECFadapter);
	}

	private void initlistenter() {
		eat_all_classify_ll.setOnClickListener(this);
		title_return_rl.setOnClickListener(this);
		order_disher_bt.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.eat_all_classify_ll:
			showChoiseDialog();
			break;
		case R.id.order_disher_bt:
			//获取所有菜品信息
			myapp.sun_money=total_money.getText().toString();
			//清空点的菜，每次不会重复加
			myapp.ChooseList.clear();
			for (int i = 0; i < myapp.disherList.size(); i++) {
				if(myapp.disherList.get(i).getNumber()>0){
					myapp.ChooseList.add(myapp.disherList.get(i));
				}
			}
			if(myapp.mark!=1){
				if(Integer.parseInt(myapp.sun_money)>0){
					Intent i = new Intent(EatChooseFoodActivity.this,EatShopCustomTableActivity.class);
					startActivity(i);
					finish();
				}else {
					ToastUtil.makeLongText(EatChooseFoodActivity.this, "请先选择菜品");
				}
			}else {
				Intent shopESR = getIntent();
				Intent i = new Intent(EatChooseFoodActivity.this,EatShopReserve.class);
				i.putExtra("title_location_tv", shopESR.getStringExtra("title_location_tv"));
				i.putExtra("peopleName", shopESR.getStringExtra("peopleName"));
				i.putExtra("peopleNumber", shopESR.getStringExtra("peopleNumber"));
				i.putExtra("show_choose_time_tv", shopESR.getStringExtra("show_choose_time_tv"));
				i.putExtra("site_tv", shopESR.getStringExtra("site_tv"));
				startActivity(i);
				finish();
			}
			break;
		case R.id.title_return_rl:
			finish();
			break;
		}
	}
	/**
	 * 选择发布的类型
	 */
	private void showChoiseDialog() {
		if (pw != null && pw.isShowing()) {
			pw.dismiss();
		} else {
			final View view = LayoutInflater.from(EatChooseFoodActivity.this).inflate(
					R.layout.layout_eat_choose_type, null);
			//得带弹出对话框的对象
			pw = new PopupWindow(view);
			ListView time_lv = (ListView)view.findViewById(R.id.time_lv);
			final ArrayList<String> typeAll = new ArrayList<String>();
			typeAll.add("热菜");typeAll.add("特色菜");typeAll.add("凉菜");typeAll.add("荤菜");
			typeAll.add("素菜");typeAll.add("特价菜");typeAll.add("小吃");typeAll.add("甜点");
			typeAll.add("茶品");typeAll.add("酒水");
			time_lv.setAdapter(new TimeChooseAdapter(typeAll, EatChooseFoodActivity.this));
			time_lv.setOnItemClickListener(new OnItemClickListener() {


				@Override
				public void onItemClick(AdapterView<?> arg0, View arg1,
						int arg2, long arg3) {
					eat_food_classify_tv.setText(typeAll.get(arg2));
					pw.dismiss();
				}
			});
			/**
			 * 这里设置显示PopuWindow之后在外面点击是否有效。
			 * 如果为false的话，那么点击PopuWindow外面并不会关闭PopuWindow。
			 * 当然这里很明显只能在Touchable下才能使用。
			 */
			pw.setOutsideTouchable(true);
			/**
			 * 当有mPop.setFocusable(false);的时候，
			 * 说明PopuWindow不能获得焦点，即使设置设置了背景不为空也不能点击外面消失，
			 * 只能由dismiss()消失，但是外面的View的事件还是可以触发,back键也可以顺利dismiss掉。
			 * 当设置为popuWindow.setFocusable(true);的时候，加上下面两行设置背景代码，点击外面和Back键才会消失。
				mPop.setFocusable(true);
				需要顺利让PopUpWindow dimiss（即点击PopuWindow之外的地方此或者back键PopuWindow会消失）；
				PopUpWindow的背景不能为空。
				必须在popuWindow.showAsDropDown(v);
				或者其它的显示PopuWindow方法之前设置它的背景不为空：
			 */
			pw.setFocusable(true);
			//点击外部关闭对话框
			view.setOnTouchListener(new OnTouchListener() {
				@Override
				public boolean onTouch(View v, MotionEvent event) {
					int with = view.findViewById(R.id.time_choose_ll).getLeft();
					int x = (int) event.getX();
					if (x > with) {
						pw.dismiss();
					}
					return false;
				}
			});
			// 此行代码可以在返回键按下的时候,使pw消失.(必须给背景)
			pw.setBackgroundDrawable(new BitmapDrawable());
			//设置动画样式
			pw.setAnimationStyle(R.style.AnimBottom);
			pw.setWidth((int) getResources().getDimension(R.dimen.x107));
			pw.setHeight(LayoutParams.WRAP_CONTENT);
			//放在特定控件下方
			pw.showAsDropDown(eat_all_classify_ll);
		}
	}
}
