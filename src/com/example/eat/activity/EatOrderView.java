package com.example.eat.activity;

import java.io.Serializable;
import java.util.ArrayList;

import com.example.eat.adapter.EatOrderAdapter;
import com.example.entity.Order;
import com.example.seeksubscribe.R;
import com.example.seeksubscribe.Activity.HomeActivity;

import android.content.Intent;
import android.sax.StartElementListener;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;


public class EatOrderView {
	private HomeActivity homeActivity;
	//这个view相当于他自身
	public View view;
	private ArrayList<Order> eatorder;
	private ListView order_list;

	public EatOrderView(HomeActivity homeActivity, View view, ArrayList<Order> order){
		this.homeActivity=homeActivity;
		this.view=view;
		this.eatorder=order;
		findview();
		initdata();
		init();
		listenter();
	}

	private void findview() {
		order_list=(ListView)view.findViewById(R.id.order_list);
	}

	private void initdata() {
		
	}

	private void init() {
		EatOrderAdapter eatatapter = new EatOrderAdapter(homeActivity, eatorder);
		order_list.setAdapter(eatatapter);
	}

	private void listenter() {
		order_list.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				Order eatOrder = eatorder.get(position);
				Intent i = new Intent(homeActivity, ActivityEatOrderOK.class);
				i.putExtra("eatOrder", (Serializable)eatOrder);
				homeActivity.startActivity(i);
			}
		});
		
	}
}
