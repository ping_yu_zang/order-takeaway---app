package com.example.eat.adapter;

import java.util.List;

import com.example.dao.MyApplication;
import com.example.entity.ShopEvent;
import com.example.seeksubscribe.R;
import com.lidroid.xutils.BitmapUtils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class EatShopEventAdapter extends BaseAdapter{

	private List<ShopEvent> list;
	private LayoutInflater inflater;
	private Context c;

	public EatShopEventAdapter(Context c,List<ShopEvent> list){
		this.list=list;
		this.c=c;
		inflater=LayoutInflater.from(c);
 	}
	@Override
	public int getCount() {
		return list.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return list.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHandle handle=null;
		if (convertView==null) {
			handle=new ViewHandle();
			convertView=inflater.inflate(R.layout.list_item, null);
			handle.event_image=(ImageView)convertView.findViewById(R.id.event_image);
			handle.eat_name_tv=(TextView)convertView.findViewById(R.id.eat_name_tv);
			handle.riginalPrice_tv=(TextView)convertView.findViewById(R.id.riginalPrice_tv);
			handle.goingPrice_tv=(TextView)convertView.findViewById(R.id.goingPrice_tv);
			convertView.setTag(handle);
		}else {
			handle=(ViewHandle) convertView.getTag();
		}
		ShopEvent sEvent=list.get(position);
		BitmapUtils b = MyApplication.getBitmapUtils(c);
		b.display(handle.event_image, sEvent.getEventImag());
		b.configDefaultBitmapConfig(Bitmap.Config.RGB_565);//设置图片压缩类型
		b.configDefaultLoadFailedImage(R.drawable.failure);//加载失败图片  
		b.configDefaultLoadingImage(R.drawable.moren);//默认背景图片  
		handle.eat_name_tv.setText(sEvent.getEatName());
		handle.riginalPrice_tv.setText(sEvent.getRiginalPrice());
		handle.riginalPrice_tv.getPaint().setFlags(Paint.STRIKE_THRU_TEXT_FLAG); //中划线
		handle.goingPrice_tv.setText(sEvent.getGoingPrice());
		return convertView;
	}
	class  ViewHandle{
		ImageView event_image;
		TextView eat_name_tv;
		TextView riginalPrice_tv;
		TextView goingPrice_tv;
	}
}
