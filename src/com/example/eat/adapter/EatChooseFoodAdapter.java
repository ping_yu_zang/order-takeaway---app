package com.example.eat.adapter;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Paint;
import android.test.TouchUtils;
import android.text.method.Touch;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.dao.MyApplication;
import com.example.eat.activity.EatChooseFoodActivity;
import com.example.eat.activity.EatShopReserve;
import com.example.entity.Dishes;
import com.example.seeksubscribe.R;
import com.example.utils.ToastUtil;
import com.example.utils.Utils;



public class EatChooseFoodAdapter extends BaseAdapter{

	private LayoutInflater inflater;
	private EatChooseFoodActivity context;
	private MyApplication myapp;

	public EatChooseFoodAdapter(EatChooseFoodActivity eatShopReserve){
		this.context= eatShopReserve;
		inflater=LayoutInflater.from(eatShopReserve);
		myapp=MyApplication.getInstance();
	}
	@Override
	public int getCount() {
		return myapp.disherList.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return myapp.disherList.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		ViewHander holder=null;
		if(convertView==null){
			convertView=inflater.inflate(R.layout.item_eat_choose_food, null);
			holder=new ViewHander();
			holder.shop_Name_tv=(TextView)convertView.findViewById(R.id.shop_Name_tv);
			holder.eat_xianjia_tv=(TextView)convertView.findViewById(R.id.eat_xianjia_tv);
			holder.yuanjia_tv=(TextView)convertView.findViewById(R.id.yuanjia_tv);
			holder.number_tv=(TextView)convertView.findViewById(R.id.number_tv);
			holder.jia_bt=(Button)convertView.findViewById(R.id.jia_bt);
			holder.jian_bt=(Button)convertView.findViewById(R.id.jian_bt);
			holder.shop_pathPicture_iv=(ImageView)convertView.findViewById(R.id.shop_pathPicture1_iv);
			convertView.setTag(holder);
		}else {
			holder=(ViewHander) convertView.getTag();
		}
		holder.shop_Name_tv.setText(myapp.disherList.get(position).getName());
		holder.eat_xianjia_tv.setText(myapp.disherList.get(position).getCurrentCost());
		holder.yuanjia_tv.getPaint().setFlags(Paint.STRIKE_THRU_TEXT_FLAG|Paint.ANTI_ALIAS_FLAG);
		holder.yuanjia_tv.setText(myapp.disherList.get(position).getOriginalCost());
		holder.number_tv.setText(myapp.disherList.get(position).getNumber()+"");
		//获取图片路径后设置显示图片ImageLorder
		//		holder.shop_pathPicture_iv.setText(list.get(position).getName());
		holder.jia_bt.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				int number=myapp.disherList.get(position).getNumber();
				number++;
				myapp.disherList.get(position).setNumber(number);
				notifyDataSetChanged();
				int current=Integer.parseInt(myapp.disherList.get(position).getCurrentCost());
				context.test(current,1);
			}
		});
		holder.jian_bt.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				int number=myapp.disherList.get(position).getNumber();
				int current=Integer.parseInt(myapp.disherList.get(position).getCurrentCost());
				if(number!=0){
					number--;
					context.test(current,2);
				}else {
					ToastUtil.makeText(context, "您还没点这道菜，喜欢可以点哦！", 1);
				}
				myapp.disherList.get(position).setNumber(number);
				notifyDataSetChanged();
			}
		});
		return convertView;
	}
	class ViewHander{
		TextView shop_Name_tv;
		TextView eat_xianjia_tv;
		TextView yuanjia_tv;
		TextView number_tv;
		Button jia_bt;
		Button jian_bt;
		//菜品图片
		ImageView shop_pathPicture_iv;

	}

}
