package com.example.eat.adapter;

import java.util.ArrayList;

import com.example.entity.ShopLv;
import com.example.seeksubscribe.R;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class TimeChooseAdapter extends BaseAdapter{


	private ArrayList<String> list;
	private LayoutInflater inflater;

	public TimeChooseAdapter(ArrayList<String> typeAll,Context context) {
		this.list=typeAll;
		inflater=LayoutInflater.from(context);
	}
	@Override
	public int getCount() {
		return list.size();
	}

	@Override
	public Object getItem(int arg0) {
		return list.get(arg0);
	}

	@Override
	public long getItemId(int arg0) {
		return arg0;
	}

	@Override
	public View getView(int position, View contentView, ViewGroup parent) {
		ViewHander hander=null;
		if(contentView==null){
			contentView=inflater.inflate(R.layout.item_layout_time_choose, null);
			hander=new ViewHander();
			hander.choose_time_tv=(TextView) contentView.findViewById(R.id.choose_time_tv);
			contentView.setTag(hander);
		}else {
			hander=(ViewHander) contentView.getTag();
		}
		hander.choose_time_tv.setText(list.get(position).toString());
		return contentView;
	}
	class ViewHander{
		TextView choose_time_tv;
	}
}
