package com.example.fun.activity;

import java.io.Serializable;
import java.util.ArrayList;

import com.example.eat.activity.ActivityEatOrderOK;
import com.example.eat.adapter.EatOrderAdapter;
import com.example.entity.Order;
import com.example.fun.adapter.FunOrderAdapter;
import com.example.seeksubscribe.R;
import com.example.seeksubscribe.Activity.HomeActivity;

import android.content.Intent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.AdapterView.OnItemClickListener;


public class FunOrderView {
	private HomeActivity homeActivity;
	public View view;
	private ArrayList<Order> funorder;
	private ListView order_list;

	public FunOrderView(HomeActivity homeActivity, View view, ArrayList<Order> order){
		this.homeActivity=homeActivity;
		this.view=view;
		this.funorder=order;
		findview();
		initdata();
		init();
		listener();
	}
	private void findview() {
		order_list=(ListView)view.findViewById(R.id.order_list);
	}

	private void initdata() {
		
	}

	private void init() {
		FunOrderAdapter funatapter = new FunOrderAdapter(homeActivity, funorder);
		order_list.setAdapter(funatapter);
	}

	private void listener() {
		order_list.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				Order funoOrder = funorder.get(position);
				Intent i = new Intent(homeActivity, ActivityFunOrderOK.class);
				i.putExtra("funoOrder", (Serializable)funoOrder);
				homeActivity.startActivity(i);
			}
		});
		
	}
}
