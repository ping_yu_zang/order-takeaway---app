package com.example.fun.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.eat.activity.EatSeekShopActivity;
import com.example.entity.Order;
import com.example.seeksubscribe.R;
import com.example.seeksubscribe.Activity.BaseActivity;
import com.example.sing.activity.ActivitySingOrderOK;

public class ActivityFunOrderOK extends BaseActivity{

	private TextView title_location_tv;
	private ImageView title_reight_iv;
	private ImageView title_toreight_iv;
	private RelativeLayout title_return_rl;
	private TextView pay_type_tv;
	private TextView consumption_time_tv;
	private TextView pay_money_tv;
	private TextView shop_name_tv;
	private TextView phone_number_tv;
	private TextView order_number_tv;
	private RelativeLayout shop_rl;
	private Button pay_ok;
	private Order funoOrder;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		requestWindowFeature(1);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_fun_order);
		findview();
		init();
		initdata();
		listenter();
	}

	private void findview() {
		//标题文字
		title_location_tv=(TextView)findViewById(R.id.title_location_tv);
		//从右往左第1个图标（收藏）
		title_reight_iv=(ImageView)findViewById(R.id.title_reight_iv);
		//从右往左第2个图标（分享）
		title_toreight_iv=(ImageView)findViewById(R.id.title_toreight_iv);
		//返回
		title_return_rl=(RelativeLayout)findViewById(R.id.title_return_rl);
		//消费状态
		pay_type_tv=(TextView)findViewById(R.id.pay_type_tv);
		//消费时间
		consumption_time_tv=(TextView)findViewById(R.id.consumption_time_tv);
		//消费金额
		pay_money_tv=(TextView)findViewById(R.id.pay_money_tv);
		//饭店名字
		shop_name_tv=(TextView)findViewById(R.id.shop_name_tv);
		//消费人手机
		phone_number_tv=(TextView)findViewById(R.id.phone_number_tv);
		//订单号
		order_number_tv=(TextView)findViewById(R.id.order_number_tv);
		//点击店铺跳转
		shop_rl=(RelativeLayout)findViewById(R.id.shop_rl);
		//去支付
		pay_ok=(Button)findViewById(R.id.pay_ok);
	}
	private void init() {
		title_location_tv.setText("订单明细");
		title_reight_iv.setBackgroundResource(R.drawable.share_nor);
		title_toreight_iv.setVisibility(View.GONE);
	}

	private void initdata() {
		funoOrder=(Order)getIntent().getSerializableExtra("funoOrder");
		pay_type_tv.setText("已消费");
		//消费时间
		consumption_time_tv.setText(funoOrder.getPay_time()+"");
		//消费金额
		pay_money_tv.setText(funoOrder.getPat_reality_mone_tv()+"");
		//饭店名字
		shop_name_tv.setText(funoOrder.getShop_name_tv()+"");
		//消费人手机
		phone_number_tv.setText(funoOrder.getPeople_phone_tv()+"");
		//订单号
		order_number_tv.setText(funoOrder.getOrder_number_tv()+"");
	}


	private void listenter() {
		title_return_rl.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
			}
		});
		shop_rl.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent i = new Intent(ActivityFunOrderOK.this, EatSeekShopActivity.class);
				//传的店铺id要换
				Bundle shopbundle = new Bundle();
				shopbundle.putInt("shopld",1);
				i.putExtra("shopbundle", shopbundle);
				startActivity(i);
			}
		});
	}
}
