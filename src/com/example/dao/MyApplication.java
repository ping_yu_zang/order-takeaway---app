package com.example.dao;

import java.io.File;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;







import android.app.Activity;
import android.app.Application;
import android.content.Context;

import com.example.entity.AddressLocation;
import com.example.entity.Dishes;
import com.example.sa.http.PublicRTHttpUtils;
import com.example.sa.http.publicData;
import com.example.seeksubscribe.Activity.AddressLocateActivity;
import com.example.utils.ConstantsUtil;
import com.example.utils.CrashHandler;
import com.lidroid.xutils.BitmapUtils;
import com.lidroid.xutils.HttpUtils;
import com.nostra13.universalimageloader.cache.disc.impl.UnlimitedDiscCache;
import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.nostra13.universalimageloader.cache.memory.impl.UsingFreqLimitedMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;
import com.nostra13.universalimageloader.core.download.BaseImageDownloader;
import com.nostra13.universalimageloader.utils.StorageUtils;





public class MyApplication extends Application {
	public static MyApplication instance;
	private List<Activity> activitys = new LinkedList<Activity>();
	//所有的菜品集合
	public ArrayList<Dishes> disherList=new ArrayList<Dishes>();
	//客户点的菜品集合
	public ArrayList<Dishes> ChooseList=new ArrayList<Dishes>();
	//总价格
	public String sun_money;
	//默认为0
	//1-EatShopReserve跳EatChooseFoodActivity（修改点菜）
	//2-重新获取菜品信息
	public int mark=0;
	//网络请求
	public static HttpUtils http;
	//定位获取的内容
	public static AddressLocation addl=null;
	//获取图片操作工具对象
	public static BitmapUtils b=null;


	public static MyApplication getInstance() {
		if(instance==null){
			instance=new MyApplication();
		}
		return instance;
	}

	// 程序入口点，如果你要重写这个方法必须调用super.onCreate().
	@Override
	public void onCreate() {
		super.onCreate();
		// 注册全局处理异常.
		CrashHandler crashHandler = CrashHandler.getInstance();
		crashHandler.init(getApplicationContext());
		initConfig(getApplicationContext());
		//创建网络连接类
		if (http==null) {
			http = PublicRTHttpUtils.getHttpUtils();
		}
	}
	/**
	 * 获取图片操作工具对象
	 */
	public static BitmapUtils getBitmapUtils(Context c){
		if (b==null) {
			b=new BitmapUtils(c);
		}
		return b;
	} 
	/**
	 * 
	 * 
	 * @param context
	 */
	public static void initConfig(Context context) {
		File cacheDir = StorageUtils.getOwnCacheDirectory(context, ConstantsUtil.SDCARD_PATH+"/cache/");  
		@SuppressWarnings("deprecation")
		ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(
				context)
		.memoryCacheExtraOptions(480, 800)
		// max width, max height，即保存的每个缓存文件的最大长宽
		// it)/设置缓存的详细信息，最好不要设置这个
		.threadPoolSize(3)
		// .推荐1-5
		// 线程池内加载的数量
		.threadPriority(Thread.NORM_PRIORITY - 2)
		.denyCacheImageMultipleSizesInMemory()
		.memoryCache(new UsingFreqLimitedMemoryCache(4 * 1024 * 1024))
		// You can pass your own memory cache
		// implementation/你可以通过自己的内存缓存实现
		.memoryCacheSize(4 * 1024 * 1024)
		.diskCacheSize(50 * 1024 * 1024)
		.discCache(new UnlimitedDiscCache(cacheDir))//自定义缓存路径  
		.diskCacheFileNameGenerator(new Md5FileNameGenerator())
		// 将保存的时候的URI名称用MD5 加密
		.tasksProcessingOrder(QueueProcessingType.LIFO)
		// 自定义缓存路径
		.defaultDisplayImageOptions(DisplayImageOptions.createSimple())
		.imageDownloader(
				new BaseImageDownloader(context, 5 * 1000, 30 * 1000)) // connectTimeout
				// (5
				// s),
				// readTimeout
				// (30
				// s)超时时间
				.writeDebugLogs() // Remove for release app
				.build();// 开始构建
		ImageLoader.getInstance().init(config);
	}
	// 添加Activity到容器中
	public void addActivity(Activity activity) {
		if (activitys != null && activitys.size() > 0) {
			if (!activitys.contains(activity)) {
				activitys.add(activity);
			}
		} else {
			activitys.add(activity);
		}
		activitys.add(activity);
	}
	//退出程序
	public void exit() {
		if (activitys != null && activitys.size() > 0) {
			for (Activity activity : activitys) {
				activity.finish();
			}
		}
	}
}
