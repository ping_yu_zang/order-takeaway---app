package com.example.entity;

import android.os.Bundle;

import com.example.dao.MyApplication;

public class AddressLocation {
	//获取位置信息
	//纬度
	double geoLat;
	//经度
	double geoLng;   
	//获取定位速度
	float speed;
	//方向
	float bearing;
	//省名称
	String province;
	//城市名称
	String city;
	//城市编码
	String code;
	//区（县）名称
	String district;
	//区域编码
	String adcode;
	//街道和门牌信息
	String street;
	//详细地址
	String address;
	//描述信息
	Bundle extras;
	public double getGeoLat() {
		return geoLat;
	}
	public void setGeoLat(double geoLat) {
		this.geoLat = geoLat;
	}
	public double getGeoLng() {
		return geoLng;
	}
	public void setGeoLng(double geoLng) {
		this.geoLng = geoLng;
	}
	public float getSpeed() {
		return speed;
	}
	public void setSpeed(float speed) {
		this.speed = speed;
	}
	public float getBearing() {
		return bearing;
	}
	public void setBearing(float bearing) {
		this.bearing = bearing;
	}
	public String getProvince() {
		return province;
	}
	public void setProvince(String province) {
		this.province = province;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getDistrict() {
		return district;
	}
	public void setDistrict(String district) {
		this.district = district;
	}
	public String getAdcode() {
		return adcode;
	}
	public void setAdcode(String adcode) {
		this.adcode = adcode;
	}
	public String getStreet() {
		return street;
	}
	public void setStreet(String street) {
		this.street = street;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public Bundle getExtras() {
		return extras;
	}
	public void setExtras(Bundle extras) {
		this.extras = extras;
	}
	@Override
	public String toString() {
		return "AddressLocation [geoLat=" + geoLat + ", geoLng=" + geoLng
				+ ", speed=" + speed + ", bearing=" + bearing + ", province="
				+ province + ", city=" + city + ", code=" + code
				+ ", district=" + district + ", adcode=" + adcode + ", street="
				+ street + ", address=" + address + ", extras=" + extras + "]";
	}
	
}
