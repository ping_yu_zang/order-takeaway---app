package com.example.entity;

import java.io.Serializable;
/**
 * 订单实体类
 * @author User
 *
 */
public class Order implements Serializable{
	String shop_name_tv;
	String order_id;
	String pay_type_tv;
	String pay_time;
	String pay_money_tv;
	String shop_img;
	String pat_reality_mone_tv;
	String people_phone_tv;
	String order_number_tv;
	String address_receiving;
	public String getShop_name_tv() {
		return shop_name_tv;
	}
	public void setShop_name_tv(String shop_name_tv) {
		this.shop_name_tv = shop_name_tv;
	}
	public String getOrder_id() {
		return order_id;
	}
	public void setOrder_id(String order_id) {
		this.order_id = order_id;
	}
	public String getPay_type_tv() {
		return pay_type_tv;
	}
	public void setPay_type_tv(String pay_type_tv) {
		this.pay_type_tv = pay_type_tv;
	}
	public String getPay_time() {
		return pay_time;
	}
	public void setPay_time(String pay_time) {
		this.pay_time = pay_time;
	}
	public String getPay_money_tv() {
		return pay_money_tv;
	}
	public void setPay_money_tv(String pay_money_tv) {
		this.pay_money_tv = pay_money_tv;
	}
	public String getShop_img() {
		return shop_img;
	}
	public void setShop_img(String shop_img) {
		this.shop_img = shop_img;
	}
	public String getPat_reality_mone_tv() {
		return pat_reality_mone_tv;
	}
	public void setPat_reality_mone_tv(String pat_reality_mone_tv) {
		this.pat_reality_mone_tv = pat_reality_mone_tv;
	}
	public String getPeople_phone_tv() {
		return people_phone_tv;
	}
	public void setPeople_phone_tv(String people_phone_tv) {
		this.people_phone_tv = people_phone_tv;
	}
	public String getOrder_number_tv() {
		return order_number_tv;
	}
	public void setOrder_number_tv(String order_number_tv) {
		this.order_number_tv = order_number_tv;
	}
	public String getAddress_receiving() {
		return address_receiving;
	}
	public void setAddress_receiving(String address_receiving) {
		this.address_receiving = address_receiving;
	}
	@Override
	public String toString() {
		return "Order [shop_name_tv=" + shop_name_tv + ", order_id=" + order_id
				+ ", pay_type_tv=" + pay_type_tv + ", pay_time=" + pay_time
				+ ", pay_money_tv=" + pay_money_tv + ", shop_img=" + shop_img
				+ ", pat_reality_mone_tv=" + pat_reality_mone_tv
				+ ", people_phone_tv=" + people_phone_tv + ", order_number_tv="
				+ order_number_tv + ", address_receiving=" + address_receiving
				+ "]";
	}

	
	
}
