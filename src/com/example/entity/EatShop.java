package com.example.entity;

import android.graphics.Bitmap;
import android.widget.ImageView;
/**
 * 餐厅实体类
 * @author User
 *
 */
public class EatShop {
	private String id;
	private String name;
	//地址
	private String address;
	//电话号码
	private String number;
	//评论人姓名
	private String commentName;
	//评论内容
	private String commentContent;
	//10分满意
	private String point;
	//头图
	private String image;
	//头图说明
	private String explain;
	//店铺介绍
	private String introduce;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getNumber() {
		return number;
	}
	public void setNumber(String number) {
		this.number = number;
	}
	public String getCommentName() {
		return commentName;
	}
	public void setCommentName(String commentName) {
		this.commentName = commentName;
	}
	public String getCommentContent() {
		return commentContent;
	}
	public void setCommentContent(String commentContent) {
		this.commentContent = commentContent;
	}
	public String getPoint() {
		return point;
	}
	public void setPoint(String point) {
		this.point = point;
	}
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
	public String getExplain() {
		return explain;
	}
	public void setExplain(String explain) {
		this.explain = explain;
	}
	public String getIntroduce() {
		return introduce;
	}
	public void setIntroduce(String introduce) {
		this.introduce = introduce;
	}
	public EatShop() {
		super();
	}
	
	public EatShop(String id, String name, String address, String number,
			String commentName, String commentContent, String point,
			String image, String explain, String introduce) {
		super();
		this.id = id;
		this.name = name;
		this.address = address;
		this.number = number;
		this.commentName = commentName;
		this.commentContent = commentContent;
		this.point = point;
		this.image = image;
		this.explain = explain;
		this.introduce = introduce;
	}
	//将来还得写个JSON解析方法
	
}
