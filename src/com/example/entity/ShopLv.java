package com.example.entity;

import java.io.Serializable;

/**
 * 店铺实体类
 * @author User
 *
 */
public class ShopLv implements Serializable{
		//店铺id
		private String shopId;
		//店铺名字
		private String shopName;
		//店铺种类
		private String shopType;
		//人均消费
		private String shopMoney;
		//距离多远（M）转换成（KM）
		private String shopRange;
		//店铺图片
		private String pathPicture;
		public String getShopId() {
			return shopId;
		}
		public void setShopId(String shopId) {
			this.shopId = shopId;
		}
		public String getShopName() {
			return shopName;
		}
		public void setShopName(String shopName) {
			this.shopName = shopName;
		}
		public String getShopType() {
			return shopType;
		}
		public void setShopType(String shopType) {
			this.shopType = shopType;
		}
		public String getShopMoney() {
			return shopMoney;
		}
		public void setShopMoney(String shopMoney) {
			this.shopMoney = shopMoney;
		}
		public String getShopRange() {
			return shopRange;
		}
		public void setShopRange(String shopRange) {
			this.shopRange = shopRange;
		}
		public String getPathPicture() {
			return pathPicture;
		}
		public void setPathPicture(String pathPicture) {
			this.pathPicture = pathPicture;
		}
		public ShopLv(String shopId, String shopName, String shopType,
				String shopMoney, String shopRange, String pathPicture) {
			super();
			this.shopId = shopId;
			this.shopName = shopName;
			this.shopType = shopType;
			this.shopMoney = shopMoney;
			this.shopRange = shopRange;
			this.pathPicture = pathPicture;
		}
		public ShopLv() {
			super();
		}
		
}
