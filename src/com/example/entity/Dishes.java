package com.example.entity;

import java.io.Serializable;
/**
 * 菜品实体类
 * @author User
 *
 */
public class Dishes implements Serializable{
	//菜品类型
	String type;
	//菜品名字
	String name;
	//菜品ID
	String id;
	//原价
	String originalCost;
	//现价
	String currentCost;
	//数量
	int number;
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getOriginalCost() {
		return originalCost;
	}
	public void setOriginalCost(String originalCost) {
		this.originalCost = originalCost;
	}
	public String getCurrentCost() {
		return currentCost;
	}
	public void setCurrentCost(String currentCost) {
		this.currentCost = currentCost;
	}
	public int getNumber() {
		return number;
	}
	public void setNumber(int number) {
		this.number = number;
	}
	
}
