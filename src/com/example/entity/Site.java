package com.example.entity;

import java.io.Serializable;

//地址实体类
public class Site implements Serializable{
	String SiteID;
	String SitePeopleName;
	String SiteTel;
	String Site;
	public String getSiteID() {
		return SiteID;
	}
	public void setSiteID(String siteID) {
		SiteID = siteID;
	}
	public String getSitePeopleName() {
		return SitePeopleName;
	}
	public void setSitePeopleName(String sitePeopleName) {
		SitePeopleName = sitePeopleName;
	}
	public String getSiteTel() {
		return SiteTel;
	}
	public void setSiteTel(String siteTel) {
		SiteTel = siteTel;
	}
	public String getSite() {
		return Site;
	}
	public void setSite(String site) {
		Site = site;
	}
	
	
}
