package com.example.entity;

import java.io.Serializable;
/**
 * 下订单人实体类
 * @author User
 *
 */
public class OrderPeople implements Serializable{
	String title_location_tv;
	String peopleName;
	String peopleNumber;
	String show_choose_time_tv;
	String gender;
	public String getTitle_location_tv() {
		return title_location_tv;
	}
	public void setTitle_location_tv(String title_location_tv) {
		this.title_location_tv = title_location_tv;
	}
	public String getPeopleName() {
		return peopleName;
	}
	public void setPeopleName(String peopleName) {
		this.peopleName = peopleName;
	}
	public String getPeopleNumber() {
		return peopleNumber;
	}
	public void setPeopleNumber(String peopleNumber) {
		this.peopleNumber = peopleNumber;
	}
	public String getShow_choose_time_tv() {
		return show_choose_time_tv;
	}
	public void setShow_choose_time_tv(String show_choose_time_tv) {
		this.show_choose_time_tv = show_choose_time_tv;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	
}
