package com.example.seeksubscribe.Activity;

import com.example.entity.Site;
import com.example.seeksubscribe.R;
import com.example.utils.ToastUtil;
import com.lidroid.xutils.view.annotation.event.OnChildClick;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class AddSiteToActivity extends BaseActivity implements OnClickListener{

	private TextView title_location_tv;
	private ImageView title_reight_iv;
	private ImageView title_toreight_iv;
	private RelativeLayout title_return_rl;
	private EditText people_name_et;
	private EditText phone_number_et;
	private Button add_site_bt;
	private TextView city_number_et;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		requestWindowFeature(1);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_add_site);
		findviewByid();
		intitData();
		init();
		initlistenter();
	}
	private void findviewByid() {
		//标题文字
		title_location_tv=(TextView)findViewById(R.id.title_location_tv);
		//从右往左第1个图标（收藏）
		title_reight_iv=(ImageView)findViewById(R.id.title_reight_iv);
		//从右往左第2个图标（分享）
		title_toreight_iv=(ImageView)findViewById(R.id.title_toreight_iv);
		//返回
		title_return_rl=(RelativeLayout)findViewById(R.id.title_return_rl);
		//配送名字
		people_name_et=(EditText)findViewById(R.id.people_name_et);
		//配送电话
		phone_number_et=(EditText)findViewById(R.id.phone_number_et);
		//配送地址
		city_number_et=(TextView)findViewById(R.id.city_number_et);
		//保存
		add_site_bt=(Button)findViewById(R.id.add_site_bt);

	}

	private void intitData() {
		city_number_et.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent i = new Intent(AddSiteToActivity.this, AddressLocateActivity.class);
				startActivityForResult(i, 100);
			}
		});

	}
	 @Override
     protected  void onActivityResult(int requestCode, int resultCode, Intent data)  {
                 super.onActivityResult(requestCode, resultCode,  data);
                 Log.i("地图得到的位置",data.getBundleExtra("address").getString("address"));
                 city_number_et.setText(data.getBundleExtra("address").getString("address"));
                 
	 }
	private void init() {
		title_location_tv.setText("添加配送地址");
		title_reight_iv.setVisibility(View.GONE);
		title_toreight_iv.setVisibility(View.GONE);

	}

	private void initlistenter() {
		title_return_rl.setOnClickListener(this);
		add_site_bt.setOnClickListener(this);

	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.title_return_rl:
			finish();
			break;
		case R.id.add_site_bt:
			Site site = new Site();
			site.setSite(city_number_et.getText().toString());
			site.setSitePeopleName(people_name_et.getText().toString());
			site.setSiteTel(phone_number_et.getText().toString());
			Intent i = new Intent();
			i.putExtra("site", site);
			setResult(1, i);
			//写入数据库
			//打开rss.db数据库，如果没有测创建，有测打开
			SQLiteDatabase db= openOrCreateDatabase("site.db", Context.MODE_PRIVATE, null);
			//sql语句
			String sql="insert into addsite (name,phone,city,site) values('"+people_name_et.getText().toString()+"','"+phone_number_et.getText().toString()+"','"+city_number_et.getText().toString()+"','"+city_number_et.getText().toString()+"')";
			System.out.println("RSS向数据库添加语句===>>"+sql);
			//发送添加语句
			db.execSQL(sql);
			//关闭数据库链接
			db.close();
			finish();
			break;

		}
		
	}
	
}
