package com.example.seeksubscribe.Activity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.example.seeksubscribe.R;
import com.example.seeksubscribe.adapter.MessageAdapter;

import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class MessageActivity extends BaseActivity{

	private ListView message_lv;
	private RelativeLayout title_return_rl;
	private TextView title_location_tv;
	private ImageView title_toreight_iv;
	private ImageView title_reight_iv;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		requestWindowFeature(1);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_message);
		findView();
		init();
		listener();
	}

	private void findView() {
		message_lv=(ListView)findViewById(R.id.message_lv);
		title_return_rl=(RelativeLayout)findViewById(R.id.title_return_rl);
		title_location_tv=(TextView)findViewById(R.id.title_location_tv);
		title_toreight_iv=(ImageView)findViewById(R.id.title_toreight_iv);
		title_reight_iv=(ImageView)findViewById(R.id.title_reight_iv);
	}

	private void init() {
		title_location_tv.setText("系统消息");
		title_toreight_iv.setVisibility(View.GONE);
		title_reight_iv.setVisibility(View.GONE);
		ArrayList list=new ArrayList<Map<String, String>>();
		// 获取数据
		for (int i = 0; i < 10; i++) {
			HashMap<String, String> map = new HashMap<String, String>();
			map.put("content", "模拟系统消息");
			map.put("time", "2016-1-12 18:18");
			list.add(map);
		}
		MessageAdapter adapter = new MessageAdapter(MessageActivity.this, list);
		message_lv.setAdapter(adapter);
	}

	private void listener() {
		title_return_rl.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
			finish();
			}
		});
		
	}
}
