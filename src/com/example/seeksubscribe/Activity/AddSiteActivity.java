package com.example.seeksubscribe.Activity;

import java.util.ArrayList;

import com.example.entity.Site;
import com.example.fun.activity.ActivityFunOrderOK;
import com.example.myView.SwipeListView;
import com.example.seeksubscribe.R;
import com.example.seeksubscribe.adapter.AddSiteAdapter;
import com.example.seeksubscribe.adapter.AddSiteAdapter.onRightItemClickListener;
import com.example.seeksubscribe.adapter.MyCollectAdapter;
import com.example.utils.ToastUtil;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
/**
 * 配送地址
 * @author User
 *
 */
public class AddSiteActivity extends BaseActivity implements OnClickListener{
	private TextView title_location_tv;
	private ImageView title_reight_iv;
	private ImageView title_toreight_iv;
	private RelativeLayout title_return_rl;
	private LinearLayout add_site_ll;
	private SwipeListView add_site_lv;
	private ArrayList<Site> sitelist=new ArrayList<Site>();
	private AddSiteAdapter adapter;
	//标记是谁跳过来的
	private int mark;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		requestWindowFeature(1);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.acitvity_expressage_site);
		findviewByid();
		intitData();
		init();
		initlistenter();
	}
	private void findviewByid() {
		//标题文字
		title_location_tv=(TextView)findViewById(R.id.title_location_tv);
		//从右往左第1个图标（收藏）
		title_reight_iv=(ImageView)findViewById(R.id.title_reight_iv);
		//从右往左第2个图标（分享）
		title_toreight_iv=(ImageView)findViewById(R.id.title_toreight_iv);
		//返回
		title_return_rl=(RelativeLayout)findViewById(R.id.title_return_rl);
		//配送地址添加
		add_site_ll=(LinearLayout)findViewById(R.id.add_site_ll);
		//添加地址
		add_site_lv=(SwipeListView)findViewById(R.id.add_site_lv);
		

	}

	private void intitData() {
		//获取intent传递过来的数据
		mark=getIntent().getIntExtra("mark",0);
		//打开数据库
		SQLiteDatabase db= openOrCreateDatabase("site.db", Context.MODE_PRIVATE, null);
		//创建表    IF NOT EXISTS加这个字段有测不创建，没有才会创建
		db.execSQL("CREATE TABLE IF NOT EXISTS addsite (id INTEGER PRIMARY KEY AUTOINCREMENT, name VARCHAR,phone VARCHAR,city VARCHAR,site VARCHAR)");
		//创建好表后把别人每次输入的Rss地址保存在数据库里，删除Rss时，连数据库中的RSS地址一起删除
		Cursor c=db.rawQuery("select * from addsite", null);
		System.out.println("数据库中保存的site地址===》》"+c.toString());
		//移动到第一条记录，有记录返回true,没有false,//判断游标是否为空
		if(c.getCount()>0){
			//总记录条数
			while(c.moveToNext()){
				//移动到指定记录  name,phone,city,site
				String name=c.getString(c.getColumnIndex("name"));
				String phone=c.getString(c.getColumnIndex("phone"));
				String city=c.getString(c.getColumnIndex("city"));
				String site=c.getString(c.getColumnIndex("site"));
				Site rss = new Site();
				rss.setSitePeopleName(name);
				rss.setSiteTel(phone);
				rss.setSite(city);
				sitelist.add(rss);
			}
		}
		db.close();
	}

	private void init() {
		title_location_tv.setText("配送地址");
		title_reight_iv.setVisibility(View.GONE);
		title_toreight_iv.setVisibility(View.GONE);
		adapter = new AddSiteAdapter(AddSiteActivity.this, sitelist,add_site_lv.getRightViewWidth());
		add_site_lv.setAdapter(adapter);
	}

	private void initlistenter() {
		title_return_rl.setOnClickListener(this);
		add_site_ll.setOnClickListener(this);
		add_site_lv.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				if (mark==101) {
					Site site=sitelist.get(position);
					Intent i = new Intent();
					Bundle b = new Bundle();
					b.putSerializable("site", site);
					i.putExtra("site", b);
					setResult(101,i);
					finish();
				}
			}
		});
		adapter.setOnRightItemClickListener(new onRightItemClickListener() {

			@Override
			public void onRightItemClick(View v, final int position) {
				//打开数据库
				SQLiteDatabase db= openOrCreateDatabase("site.db", Context.MODE_PRIVATE, null);
				//删除sql语句
				String sql="delete from addsite where site='"+sitelist.get(position).getSite()+"'";
				System.out.println("RSS向数据库删除语句===>>"+sql);
				//发送添加语句
				db.execSQL(sql);
				//关闭数据库链接
				db.close();
				//删除后掉服务器接口，重新获取下数据
				sitelist.remove(position);
				adapter.notifyDataSetChanged();
				initlistenter();
			}
		});
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.title_return_rl:
			finish();
			break;
		case R.id.add_site_ll:
			Intent i = new Intent(AddSiteActivity.this, AddSiteToActivity.class);
			startActivityForResult(i, 1);
			break;

		}
		
	}
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		switch (resultCode) {
		//返回1添加地址
		case 1:
			Site site=(Site) data.getSerializableExtra("site");
			sitelist.add(site);
			adapter.notifyDataSetChanged();
			break;
		}
	}
	
}
