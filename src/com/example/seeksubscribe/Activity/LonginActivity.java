package com.example.seeksubscribe.Activity;

import com.example.seeksubscribe.R;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class LonginActivity extends BaseActivity implements OnClickListener{

	private TextView title_location_tv;
	private ImageView title_reight_iv;
	private ImageView title_toreight_iv;
	private RelativeLayout title_return_rl;
	private Button find_bt;
	private Button register_bt;
	private Button login_ok_bt;
	private EditText login_password_et;
	private EditText login_name_et;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		requestWindowFeature(1);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);
		findviewByid();
		intitData();
		init();
		initlistenter();
	}

	private void findviewByid() {
		//标题文字
		title_location_tv=(TextView)findViewById(R.id.title_location_tv);
		//从右往左第1个图标（收藏）
		title_reight_iv=(ImageView)findViewById(R.id.title_reight_iv);
		//从右往左第2个图标（分享）
		title_toreight_iv=(ImageView)findViewById(R.id.title_toreight_iv);
		//返回
		title_return_rl=(RelativeLayout)findViewById(R.id.title_return_rl);
		//登陆用户名
		login_name_et=(EditText)findViewById(R.id.login_name_et);
		//登录密码
		login_password_et=(EditText)findViewById(R.id.login_password_et);
		//登录按钮
		login_ok_bt=(Button)findViewById(R.id.login_ok_bt);
		//注册
		register_bt=(Button)findViewById(R.id.register_bt);
		//找回密码
		find_bt=(Button)findViewById(R.id.find_bt);

	}

	private void intitData() {
		

	}

	private void init() {
		title_location_tv.setText("找预订登陆");
		title_reight_iv.setVisibility(View.GONE);
		title_toreight_iv.setVisibility(View.GONE);

	}

	private void initlistenter() {
		title_return_rl.setOnClickListener(this);
		register_bt.setOnClickListener(this);
		find_bt.setOnClickListener(this);

	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.title_return_rl:
			finish();
			break;
		case R.id.register_bt:
			Intent i = new Intent(LonginActivity.this, RegisterActivity.class);
			startActivity(i);
			break;
		case R.id.find_bt:
			Intent i1 = new Intent(LonginActivity.this, FindPasswordActivity.class);
			startActivity(i1);
			break;
		}
	}
}
