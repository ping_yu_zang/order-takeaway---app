package com.example.seeksubscribe.adapter;

import java.util.List;

import com.example.seeksubscribe.R;
import com.example.seeksubscribe.adapter.AddSiteAdapter.handerView;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class AddressLocateAdapter extends BaseAdapter{

	private List<String> list;
	private LayoutInflater inflate;

	public AddressLocateAdapter(Context context,List<String> list){
		this.list=list;
		inflate=LayoutInflater.from(context);
	}
	@Override
	public int getCount() {
		
		return list.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return list.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		viewhandler handler=null;
		if(convertView==null){
			handler=new viewhandler();
			convertView=inflate.inflate(R.layout.item_address, null);
			handler.address_tv=(TextView) convertView.findViewById(R.id.address_tv);
			convertView.setTag(handler);
		}else {
			handler=(viewhandler) convertView.getTag();
		}
		handler.address_tv.setText(list.get(position));
		return convertView;
	}
	class viewhandler{
		TextView  address_tv;
	}
}
