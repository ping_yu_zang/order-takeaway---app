/*
Navicat MySQL Data Transfer

Source Server         : MySql
Source Server Version : 50162
Source Host           : localhost:3306
Source Database       : zpy

Target Server Type    : MYSQL
Target Server Version : 50162
File Encoding         : 65001

Date: 2016-04-08 13:49:03
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for cai_pin
-- ----------------------------
DROP TABLE IF EXISTS `cai_pin`;
CREATE TABLE `cai_pin` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '菜品',
  `name` varchar(50) DEFAULT NULL COMMENT '菜名',
  `pic` decimal(8,2) DEFAULT '0.00' COMMENT '价格',
  `yuan_pic` decimal(8,2) DEFAULT '0.00' COMMENT '原价',
  `img` varchar(200) DEFAULT NULL COMMENT '图片',
  `state` int(1) DEFAULT '1' COMMENT '状态',
  `cai_pin_fen_lei_id` bigint(20) DEFAULT NULL COMMENT '菜品分类id',
  `dian_pu_id` bigint(20) DEFAULT NULL COMMENT '店铺id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of cai_pin
-- ----------------------------

-- ----------------------------
-- Table structure for cai_pin_fen_lei
-- ----------------------------
DROP TABLE IF EXISTS `cai_pin_fen_lei`;
CREATE TABLE `cai_pin_fen_lei` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '菜品分类表',
  `name` varchar(20) DEFAULT NULL COMMENT '名称',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of cai_pin_fen_lei
-- ----------------------------
INSERT INTO `cai_pin_fen_lei` VALUES ('1', '热菜');
INSERT INTO `cai_pin_fen_lei` VALUES ('2', '凉菜');
INSERT INTO `cai_pin_fen_lei` VALUES ('3', '小吃');
INSERT INTO `cai_pin_fen_lei` VALUES ('4', '甜点');
INSERT INTO `cai_pin_fen_lei` VALUES ('5', '酒水');

-- ----------------------------
-- Table structure for dian_pu
-- ----------------------------
DROP TABLE IF EXISTS `dian_pu`;
CREATE TABLE `dian_pu` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '商铺',
  `fen_lei_id` bigint(20) DEFAULT NULL COMMENT '分类表（id）',
  `log` varchar(200) DEFAULT NULL COMMENT '店铺log',
  `name` varchar(50) DEFAULT NULL COMMENT '店名',
  `lei_xing_id` bigint(20) DEFAULT NULL COMMENT '店铺类型',
  `ren_jun_pic` decimal(8,2) DEFAULT NULL COMMENT '人均消费',
  `lbs` varchar(100) DEFAULT NULL COMMENT '坐标',
  `addr` varchar(200) DEFAULT NULL COMMENT '地址',
  `phone` varchar(20) DEFAULT NULL COMMENT '电话',
  `tui_jian` bigint(20) DEFAULT NULL COMMENT '菜品id',
  `dian_jia_jie_shao` varchar(500) DEFAULT '营业时间以外点餐不送不做不退款' COMMENT '商家简介',
  `dian_jia_state` int(1) DEFAULT '1' COMMENT '开启',
  `ying_ye_time` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of dian_pu
-- ----------------------------

-- ----------------------------
-- Table structure for fen_lei
-- ----------------------------
DROP TABLE IF EXISTS `fen_lei`;
CREATE TABLE `fen_lei` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '业务类型',
  `name` varchar(20) DEFAULT NULL COMMENT '名称',
  `staty` tinyint(1) DEFAULT '1' COMMENT '状态（是否开启）',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of fen_lei
-- ----------------------------
INSERT INTO `fen_lei` VALUES ('1', '订吃', '1');

-- ----------------------------
-- Table structure for guang_gao
-- ----------------------------
DROP TABLE IF EXISTS `guang_gao`;
CREATE TABLE `guang_gao` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '广告',
  `img` varchar(500) DEFAULT NULL COMMENT '图片',
  `urls` varchar(200) DEFAULT NULL COMMENT '连接',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of guang_gao
-- ----------------------------

-- ----------------------------
-- Table structure for lei_xing
-- ----------------------------
DROP TABLE IF EXISTS `lei_xing`;
CREATE TABLE `lei_xing` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '店铺类型',
  `name` varchar(50) DEFAULT NULL COMMENT '分类名称',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of lei_xing
-- ----------------------------
INSERT INTO `lei_xing` VALUES ('1', '中餐');
INSERT INTO `lei_xing` VALUES ('2', '西餐');
INSERT INTO `lei_xing` VALUES ('3', '火锅');

-- ----------------------------
-- Table structure for order
-- ----------------------------
DROP TABLE IF EXISTS `order`;
CREATE TABLE `order` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '订单',
  `dian_pu_name` varchar(20) DEFAULT NULL COMMENT '店铺名称',
  `dian_pu_log` varchar(200) DEFAULT NULL COMMENT '店铺log',
  `time` varchar(50) DEFAULT NULL COMMENT '下单时间',
  `pic` decimal(8,2) DEFAULT NULL COMMENT '订单金额',
  `zhuang_tai` int(1) DEFAULT NULL COMMENT '订单状态',
  `user_name` varchar(50) DEFAULT NULL COMMENT '用户名',
  `user_phone` varchar(20) DEFAULT NULL COMMENT '用户电话',
  `addr` varchar(500) DEFAULT NULL COMMENT '收货地址',
  `order_num` varchar(200) DEFAULT NULL COMMENT '订单编号',
  `pay_time` varchar(50) DEFAULT NULL COMMENT '付款时间',
  `over_time` varchar(50) DEFAULT NULL COMMENT '完成时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of order
-- ----------------------------

-- ----------------------------
-- Table structure for order_zhuangtai
-- ----------------------------
DROP TABLE IF EXISTS `order_zhuangtai`;
CREATE TABLE `order_zhuangtai` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '订单状态',
  `str` varchar(20) DEFAULT NULL COMMENT '状态',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of order_zhuangtai
-- ----------------------------
INSERT INTO `order_zhuangtai` VALUES ('1', '未付款');
INSERT INTO `order_zhuangtai` VALUES ('2', '已付款');
INSERT INTO `order_zhuangtai` VALUES ('3', '已完成');

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '用户信息',
  `name` varchar(50) DEFAULT NULL COMMENT '用户名',
  `pwd` varchar(200) DEFAULT NULL COMMENT '密码',
  `phone` varchar(20) DEFAULT NULL COMMENT '电话号码',
  `addrs` varchar(500) DEFAULT NULL COMMENT '地址',
  `img` varchar(200) DEFAULT NULL COMMENT '用户头像',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user
-- ----------------------------

-- ----------------------------
-- Table structure for user_shou_cang
-- ----------------------------
DROP TABLE IF EXISTS `user_shou_cang`;
CREATE TABLE `user_shou_cang` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '用户收藏',
  `dian_pu_id` bigint(20) DEFAULT NULL COMMENT '店铺id',
  `user_id` bigint(20) DEFAULT NULL COMMENT '用户id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user_shou_cang
-- ----------------------------
